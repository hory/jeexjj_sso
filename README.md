#### 技术交流
- QQ群：174266358
- 微信群：加微信forever-evolution，把你拉群,备注：进群。
- ![](https://gitee.com/zhanghejie/jeexjj/raw/master/doc/img/jeexjj_wx.jpg)
#### JEEXJJ_SSO单点登陆系统简介
- JEEXJJ_SSO借鉴CAS基于SSM快速开发框架JEEXJJ的一款单点登陆服务实现。可以帮助企业快速搭建单点登陆服务，使用户一次登陆就可以畅游于各互相信任的信息系统。
- JEEXJJ开发框架，请移步[https://gitee.com/zhanghejie/jeexjj](https://gitee.com/zhanghejie/jeexjj)。
- QQ交流群：174266358。


#### 技术栈
技术 | 名称 | 官网
----|------|----
JEEXJJ | JEEXJJ开发框架  | [https://gitee.com/zhanghejie/jeexjj](https://gitee.com/zhanghejie/jeexjj)
thymeleaf | 页面模板引擎  | [https://www.thymeleaf.org/](https://www.thymeleaf.org/)


#### 实现原理
1.名词解释

- GrantingTicket：登陆用户的唯一标识票据。（基于cookie实现,简写：GT）
- ServiceTicket：用户登陆成功后，每访问一个应用信息，都会再为它生成一个st票据。例：如果一个用户登陆了基于sso的三个互相信我任的系统。那么对应该用户总共有1个GrantingTicket，3个ServiceTicket。
2.下面通过时序图的方式来介绍sso的登陆验证流程。

- 登陆窗口在client端的登陆流程如下：
- ![](doc/img/xulietu1.png)
- 已经登陆sso的用户首次访问某信任系统时的验证流程
![](doc/img/xulietu2.png)
- 单点退出流程
- ![](doc/img/xulietu3.png)


#### 运行步骤
1. 下载jeexjj_sso项目  https://gitee.com/zhanghejie/jeexjj_sso
1. 使用jeexjj_sso/doc/db/jeexjj_sso.sql 创建单点登陆服务端的数据库
1. 分别导入两个maven项目jeexjj_sso_server和jeexjj_sso_client
1. 分别运行两个项目的Application.java的main方法启动两个项目
1. 访问服务端项目,登陆后，查看【最近在线用户】菜单，在后台查看客户端登陆情况。http://localhost:8884/sso_server，
1. 使用浏览器测试单点登陆功能，http://localhost:8885/sso_client
1. 使用另外一种浏览器测试单点登陆（jsop方式），http://localhost:8885/sso_client/passport/jsonp
1. 分别在这两个浏览器中直接访问[http://127.0.0.1:8885/sso_client/auth/home/](http://127.0.0.1:8885/sso_client/auth/home/),模拟跨域后登陆状态能否保持。

#### 服务端配置介绍（jeexjj_sso_server.properties）

```
sso.path.authentication=/sso/authentication  #单点登陆验证url，一般默认就行
sso.path.signin=/sso/signin                  #单点登陆url，一般默认就行

sso.path.signout=/sso/signout                #单点退出url，一般默认
sso.path.validateTicket=/sso/validateTicket  #ticket验证url,一般默认
sso.url.index=/sso/index.jsp                 #
sso.url.login=http://localhost:8884/sso_server  #服务端访问url,根据情况修改

sso.cookie.maxage=-1                         #cookie相关，一般默认
sso.cookie.name=XJJTC
sso.cookie.path=/

sso.server.error=ssoerror                    #登陆失败返回参数
#unit minute
sso.grantingticket.invalidate=300            #GT缓存时间（分钟）
#sso project app
sso.login.type=project                       #登陆方式

#mcmcache config 
memcache.is.use=false                        #是否使用memcache做为缓存
memcache.servers=192.168.54.223\:11211
memcache.ticket.expiry=24
memcache.user.expiry=120


#sso notification type http|rabbitmq         #通知方式（http或rabbitmq）
sso.notification.type=http
#rabbitmq config test                        #rabbit服务相关
rabbitmq.server=192.168.54.204
rabbitmq.port=5672
rabbitmq.username=admin
rabbitmq.password=admin
rabbitmq.exchange=sso_topic


#### 客户端配置介绍（jeexjj_sso_client.properties）

sso.client.sessionHandle=com.xjj.sso.client.session.SessionHandleImpl  #接口实现，客户端实现，
sso.client.projectCode=zhj                                             #项目编码，登陆服务端后台添加
sso.server.url=http://localhost:8884/jeexjj_sso                        #单点登陆服务地址

#sso client
sso.client.receipt=sso.client.receipt
sso.client.ssoLogonUrl=/jsp/ssoLogon                                   #单点登陆验证地址，可以修改，对应登陆页面的提交地址。
sso.client.loginUrl=/sso/login.jsp                                     #该系统的登陆地址
sso.client.login.isredirect=true 
sso.client.nofilter=/*/bb/*,/*login*                                   #sso排除拦截地址列表，用逗号隔开。

#sso server url config
sso.server.authentication=/sso/authentication
sso.server.validateTicket=/sso/validateTicket
sso.server.ssoLogoutUrl=/sso/signout
sso.server.signin=/sso/signin
sso.server.error=ssoerror

#sso notification type http|rabbitmq
sso.notification.type=http

#rabbitmq config
rabbitmq.server=192.168.54.204
rabbitmq.port=5672
rabbitmq.username=admin
rabbitmq.password=admin
rabbitmq.exchange=sso_topic
```

#### 运行效果截图
- ![](doc/img/sso_server.png)
- ![](doc/img/sso_client.png)
- ![](doc/img/sso_client2.png)

#### 技术交流
1. QQ群：174266358

#### JEEXJJ开源项目

 名称 | 地址
------|----
快速开发框架  | [https://gitee.com/zhanghejie/jeexjj](https://gitee.com/zhanghejie/jeexjj)
小程序商城  | [https://gitee.com/jeexjj/jeexjj_wxmall](https://gitee.com/jeexjj/jeexjj_wxmall)
VUE仿锤子商城  | [https://gitee.com/zhanghejie/jeexjj_mall](https://gitee.com/zhanghejie/jeexjj_mall)
防CAS单点登陆  | [https://gitee.com/zhanghejie/jeexjj_sso](https://gitee.com/zhanghejie/jeexjj_sso)