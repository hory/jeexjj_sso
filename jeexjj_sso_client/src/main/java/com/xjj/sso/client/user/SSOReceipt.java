package com.xjj.sso.client.user;

import java.io.Serializable;


public class SSOReceipt implements Serializable{
	private static final long serialVersionUID = 1L;
	//标识用户是否存在 0用户不存在，1密码错误,2参数非法，3项目编码错误 9用户验证通过
	private Long userId;
    private String userType;
    private String loginName;
    private String loginType;
	private String email;
	private String mobile;
    private String userName;
    //状态 用户状态
  	private String status;
	
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getLoginType() {
		return loginType;
	}
	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
    public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
    public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("loginName="+loginName);
		sb.append("&userName="+userName);
		sb.append("&userType="+userType);
		sb.append("]");
		return sb.toString();
    }
}