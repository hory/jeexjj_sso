;(function (){
	var xjjsso={};
	
	if(!window.xjjsso){
		window.xjjsso = xjjsso;
	}
	
	//=============sso signin config begin===============
	xjjsso.projectCode="zhj";
	xjjsso.server="http://localhost:8884/sso_server";
	xjjsso.ssoSigninURL="/sso/signin";
	xjjsso.ssoAuthenticationURL="/sso/authentication";
	xjjsso.ssoSignoutURL="/sso/signout";
	//=============sso signin config end===============
	
	
	
	var service = encodeURIComponent(window.location.href);
	/**
	 * formid:登陆表单的id
	 * callback:回调函数
	 */
	xjjsso.ssoLogonByJsonp = function(formId,callback)
	{
		var data = $("#"+formId).serialize();
		data+="&jsonp=jsonp&projectCode="+xjjsso.projectCode+"&service="+service;
		$.ajax({
			url : xjjsso.server+xjjsso.ssoSigninURL,
			contentType : "application/x-www-form-urlencoded; charset=UTF-8",
			dataType : 'jsonp',
			data : data,
			jsonp : 'callback',
			success:function(ssoUser){
				callback(ssoUser);
		    }
		});
	}
	
	//单点验证
	xjjsso.ssoAuthenticationByJsonp = function(callback)
	{
		var data="jsonp=jsonp&projectCode="+xjjsso.projectCode+"&service="+service;
		$.ajax({
			url : xjjsso.server+xjjsso.ssoAuthenticationURL,
			contentType : "application/x-www-form-urlencoded; charset=UTF-8",
			dataType : 'jsonp',
			data : data,
			jsonp : 'callback',
			success:function(ssoUser){
				callback(ssoUser);
		    }
		});
	}
	
	//单点退出
	xjjsso.ssoSignoutByJsonp = function(callback)
	{
		var data="jsonp=jsonp&projectCode="+xjjsso.projectCode;
		$.ajax({
			url : xjjsso.server+xjjsso.ssoSignoutURL,
			contentType : "application/x-www-form-urlencoded; charset=UTF-8",
			dataType : 'jsonp',
			data : data,
			jsonp : 'callback',
			success:function(ssoUser){
				if(callback)
				{
					callback(ssoUser);
				}
		    }
		});
	}
	
})();