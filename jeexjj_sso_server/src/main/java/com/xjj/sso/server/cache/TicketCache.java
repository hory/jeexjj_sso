package com.xjj.sso.server.cache;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.xjj.sso.server.ticket.GrantingTicket;
import com.xjj.sso.server.ticket.ServiceTicket;
import com.xjj.sso.server.util.SSOConfiguration;
import com.xjj.sso.server.util.SSOUtils;

public class TicketCache {
	
	//是否使用memcahce来缓存ticket
	private static boolean isUseMemcache  = SSOConfiguration.getBoolean("memcache.is.use",false);
	//ticket存活时间
	private static int ticketExpiry  = SSOConfiguration.getInt("memcache.ticket.expiry",24);
	
	//user_id--->gtId
	private static ConcurrentHashMap<String,String> USER_GT_CACHE_MAP = new ConcurrentHashMap<String,String>();
	//gtId--->GrantingTicket
	private static ConcurrentHashMap<String,GrantingTicket> GT_CACHE_MAP = new ConcurrentHashMap<String,GrantingTicket>();
	
	//ST对应GT缓存
	private static ConcurrentHashMap<String,String> ST_GT_CACHE_MAP = new ConcurrentHashMap<String,String>();
		
	/**
	 * 根据cookie值获得GrantingTicket
	 * @param gTicket
	 * @return
	 */
	public static GrantingTicket getGrantingTicket(String gTicket)
	{
		if(null == gTicket)
		{
			return null;
		}
		if(isUseMemcache)
		{
			GrantingTicket gt = (GrantingTicket)MemCacheUtil.get(gTicket);
			return  gt;
		}
		GrantingTicket gt = GT_CACHE_MAP.get(gTicket);
		return gt;
	}
	
	/**
	 * 缓存GrantingTicket
	 * @param grantingTicket
	 */
	public static void putGrantingTicket(GrantingTicket grantingTicket)
	{
		if(isUseMemcache)
		{
			MemCacheUtil.put(grantingTicket.getId(), grantingTicket,ticketExpiry);
			MemCacheUtil.put("user_"+grantingTicket.getUser().getUserId(),grantingTicket.getId(),ticketExpiry);
		}else
		{
			if(GT_CACHE_MAP.contains(grantingTicket.getId()))
			{
				GT_CACHE_MAP.remove(grantingTicket.getId());
			}
			GT_CACHE_MAP.put(grantingTicket.getId(), grantingTicket);
			USER_GT_CACHE_MAP.put(grantingTicket.getId(), grantingTicket.getId());
		}
	}
	
	/**
	 * 得到已登陆的用户的gtId
	 * @param userId
	 */
	public static String getSignedGtId(Long userId)
	{
		String gt = null;
		if(isUseMemcache)
		{
			gt = (String)MemCacheUtil.get("user_"+userId);
		}else
		{
			gt = USER_GT_CACHE_MAP.get("user_"+userId);
		}
		return gt;
	}
	
	/**
	 * 缓存ServiceTicket对应的GrantingTicket
	 * @param st
	 * @param gt
	 */
	public static void putServiceTicket2GrantingTicket(String st,String gt)
	{
		if(isUseMemcache)
		{
			MemCacheUtil.put(st,gt,ticketExpiry);
			
		}else
		{
			ST_GT_CACHE_MAP.put(st,gt);
		}
	}
	
	/**
	 * 使ServiceTicket失效
	 * @param st
	 */
	public static void expiredServiceTicket(String st)
	{
		
		if(isUseMemcache)
		{
			String gt = (String)MemCacheUtil.get(st);
			if(null != gt)
			{
				GrantingTicket grantingTicket = (GrantingTicket)MemCacheUtil.get(gt);
				if(null != grantingTicket)
				{
					grantingTicket.expiredServiceTicket(st);
				}
			}
		}else
		{
			String gt = null;
			if(ST_GT_CACHE_MAP.containsKey(st))
			{
				gt = ST_GT_CACHE_MAP.get(st);
			}
			if(null != gt)
			{
				GrantingTicket grantingTicket = GT_CACHE_MAP.get(gt);
				grantingTicket.expiredServiceTicket(st);
			}
		}
		
	}
	
	/**
	 * 根据ticket的值获得cookie值
	 * @param ticket
	 * @return
	 */
	public static String getGTicket(String ticket)
	{
		if(SSOUtils.isBlank(ticket))
		{
			return null;
		}
		
		if(isUseMemcache)
		{
			return (String)MemCacheUtil.get(ticket);
		}else
		{
			return ST_GT_CACHE_MAP.get(ticket);
		}
	}
	
	
	/**
	 * 从缓存中移除GrantingTicket
	 * @param gTicket
	 */
	public static void removeGrantingTicket(String gTicket)
	{
		if(SSOUtils.isBlank(gTicket))
		{
			return;
		}
		
		if(isUseMemcache)
		{
			if(null != MemCacheUtil.get(gTicket))
			{
				MemCacheUtil.remove(gTicket);
			}
		}else
		{
			if(GT_CACHE_MAP.containsKey(gTicket))
			{
				GT_CACHE_MAP.remove(gTicket);
			}
		}
	}
	
	/**
	 * 从缓存中移除ServiceTicket
	 * @param sTicket
	 */
	public static void removeServiceTicket(String sTicket)
	{
		if(SSOUtils.isBlank(sTicket))
		{
			return;
		}
		if(isUseMemcache)
		{
			if(null != MemCacheUtil.get(sTicket))
			{
				MemCacheUtil.remove(sTicket);
			}
		}else
		{
			if(ST_GT_CACHE_MAP.containsKey(sTicket))
			{
				ST_GT_CACHE_MAP.remove(sTicket);
			}
		}
		
	}
	
	/**
	 * 定时更新ticket缓存
	 */
	public static void invalidateGrantingTicketByTime()
	{
		final long  invalidateTime = SSOConfiguration.getInt("sso.grantingticket.invalidate", 600);
		
		final long currentTime = System.currentTimeMillis();
		Set<String> gtSet = GT_CACHE_MAP.keySet();
		String gTicket = null;
		GrantingTicket grantingTicket = null;
		Set<String> stSet = null;
		String sTicket = null;
		for (Iterator<String> iterator = gtSet.iterator(); iterator.hasNext();) {
			gTicket = (String) iterator.next();
			grantingTicket = GT_CACHE_MAP.get(gTicket);
			
			if(currentTime - grantingTicket.getCreationTime().getTime()>invalidateTime*60*1000)
			{
				
				if(null != grantingTicket)
				{
					ConcurrentHashMap<String, ServiceTicket> stMap = grantingTicket.getServiceTicketMap();
					stSet = stMap.keySet();
					for (Iterator<String> iterator2 = stSet.iterator(); iterator2.hasNext();) {
						sTicket = iterator2.next();
						ST_GT_CACHE_MAP.remove(sTicket);
					}
				}
				GT_CACHE_MAP.remove(gTicket);
			}
		}
	}
	
	/**
	 * 获得所有的GrantingTicket列表
	 * @return
	 */
	public static List<GrantingTicket> getAllGrantingTicket() {
		List<GrantingTicket> gtList = null;
		if(isUseMemcache)
		{
			gtList = MemCacheUtil.getAllGrantingTicket();
		}
		else
		{
			gtList = new ArrayList<GrantingTicket>();
			Set<String> gtSet = GT_CACHE_MAP.keySet();
			for (Iterator<String> iterator = gtSet.iterator(); iterator.hasNext();) {
				String gtid = iterator.next();
				gtList.add(GT_CACHE_MAP.get(gtid));
			}
		}
		return gtList;
	}
	
	
	/**
	 * 打印缓存
	 */
	public static void printCache()
	{
//		System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
//		Set<String> keySet = GT_CACHE_MAP.keySet();
//		for (Iterator<String> iterator = keySet.iterator(); iterator.hasNext();) {
//			String gt = iterator.next();
//			
//			System.out.println(GT_CACHE_MAP.get(gt));
//		}
//		System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
//		
//		
//		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
//		Set<String> keyset2 = ST_GT_CACHE_MAP.keySet();
//		
//		for (Iterator<String> iterator = keyset2.iterator(); iterator.hasNext();) {
//			String st = iterator.next();
//			System.out.println(st+"="+ST_GT_CACHE_MAP.get(st));
//		}
//		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
	}
}
