package com.xjj.sso.server.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public interface SSOHandler{
	
	/**
	 * 验证是否已经单点登录
	 * @param request
	 * @param response
	 * @param chain
	 * @throws IOException
	 * @throws ServletException
	 */
	public void authentication(HttpServletRequest request, HttpServletResponse response,
			FilterChain chain) throws IOException, ServletException;
	
	
	/**
	 * 单点登录
	 * @param request
	 * @param response
	 * @param chain
	 * @throws IOException
	 * @throws ServletException
	 */
	public void signin(HttpServletRequest request, HttpServletResponse response,
			FilterChain chain) throws IOException, ServletException;
	
	/**
	 * 单点退出
	 * @param request
	 * @param response
	 * @param chain
	 * @throws IOException
	 * @throws ServletException
	 */
	public void signout(HttpServletRequest request, HttpServletResponse response,
			FilterChain chain) throws IOException, ServletException;
	
	/**
	 * 去sso验证ticket的合法性，并返回来登陆用户信息
	 * @param request
	 * @param response
	 * @param chain
	 * @throws IOException
	 * @throws ServletException
	 */
	public void validateTicket(HttpServletRequest request, HttpServletResponse response,
			FilterChain chain) throws IOException, ServletException;
}