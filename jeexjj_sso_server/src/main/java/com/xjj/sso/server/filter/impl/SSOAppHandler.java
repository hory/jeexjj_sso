package com.xjj.sso.server.filter.impl;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xjj.sso.server.filter.SSOHandler;
import com.xjj.sso.server.filter.SSOHandlerSupport;


public class SSOAppHandler extends SSOHandlerSupport implements SSOHandler{

	
	private static SSOAppHandler ssoHandler = new SSOAppHandler();
	public static SSOHandler getSSOHandler() {
		return ssoHandler;
	}
	public void authentication(HttpServletRequest request,
			HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		
	}

	public void signin(HttpServletRequest request,
			HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		
	}

	public void signout(HttpServletRequest request,
			HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * 验证ticket
	 * @param request
	 * @param response
	 * @param chain
	 * @throws IOException
	 * @throws ServletException
	 */
	public void validateTicket(HttpServletRequest request, HttpServletResponse response,
			FilterChain chain) throws IOException, ServletException {}
	
}