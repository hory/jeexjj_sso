package com.xjj.sso.server.ticket;

import java.io.Serializable;
import java.util.Date;

public abstract class Ticket implements Serializable {

	private static final long serialVersionUID = 1L;

	String id;

    boolean expired;
    
    Date creationTime;
    
    int countOfUses;
    
	public String getId() {
		return id;
	}


	public boolean isExpired() {
		return expired;
	}


	public Date getCreationTime() {
		return creationTime;
	}


	public int getCountOfUses() {
		return countOfUses;
	}



	public void setId(String id) {
		this.id = id;
	}


	public void setExpired(boolean expired) {
		this.expired = expired;
	}


	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}


	public void setCountOfUses(int countOfUses) {
		this.countOfUses = countOfUses;
	}
}
