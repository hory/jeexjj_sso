package com.xjj.sso.server.util;

import java.io.File;
import java.io.IOException;
public class UpdateWarClassToApplication {
  private static int cnt = 0;
  private static int cnt_fail = 0;
  
  /**
   * @param args
   *          author xjj create time 11:11:01 AM
   * @throws IOException 
   */
  public static void main(String[] args) throws IOException {
    // 源路径 
    String[] sourceFileAbs = new String[] {
        	
    		"/WEB-INF/classes/com.xjj.sso.server/filter/SSOFilter.class",
    		"/WEB-INF/view/passport/learnerlogin.ftl"
           	
    };
    String source_path = "D:/workspaces10/ssov2/WebRoot";
    // 目标路径
    String destFilePathPre = "d:/zhj/sso";
    deleteFile(destFilePathPre);
    copyFile(source_path, sourceFileAbs, destFilePathPre);
    System.out.println("copy is ok ！！！" + "成功："+cnt+"；失败："+cnt_fail);
  }

  private static void deleteFile(String destFilePathPre) throws IOException {
  	FileUtil.deleteFilesAndForder(new File(destFilePathPre));
  }

  /**
   * @param sourcePre
   * @param sourceDir
   * @param destFilePre
   *          author liyin create time 11:19:43 AM
   */
  private static void copyFile(String sourcePre, String[] sourceDir, String destFilePre) {
    for (int i = 0; i < sourceDir.length; i++) {
      String sourceFile = sourceDir[i];
      File sfile = new File(sourcePre + sourceFile);
      if (!sfile.exists()) {
        System.out.println("失败copy：" + sourcePre + sourceFile + " 不存在");
        cnt_fail ++;
      } else {
        boolean b = FileUtil.copyFiles(sourcePre + sourceFile, destFilePre + sourceFile);
        if (b) {
          System.out.println("成功copy：" + sourcePre + sourceFile);
          cnt++;
        } else {
          System.out.println("失败copy：" + sourceFile);
          cnt_fail ++;
        }
      }
    }
  }
}